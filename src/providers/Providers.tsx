"use client";
import { PropsWithChildren } from "react";
import ReduxProvider from "./ReduxProvider";

export default function Providers({ children }: PropsWithChildren) {

  return (
    <ReduxProvider>
      {children}
    </ReduxProvider>
  );
}
