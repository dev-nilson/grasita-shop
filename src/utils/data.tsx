import Image from "next/image";
import { v4 as uuidv4 } from "uuid";
import nikeDunk from "../assets/nike-dunk.png";
import nikeCortez from "../assets/nike-cortez.png";
import nikeAirForce from "../assets/nike-air-force.png";
import newBalance500 from "../assets/new-balance-550.png";
import adidasForum from "../assets/adidas-forum.png";


export const sneakers = [
  {
    key: uuidv4(),
    content: <Image src={nikeDunk} alt="nike dunk" width={1000} height={600} />,
  },
  {
    key: uuidv4(),
    content: <Image src={nikeCortez} alt="nike cortez" width={1000} height={600} />,
  },
  {
    key: uuidv4(),
    content: <Image src={nikeAirForce} alt="nike air force" width={1000} height={600} />,
  },
  {
    key: uuidv4(),
    content: <Image src={newBalance500} alt="new balance 550" width={1000} height={600} />,
  },
  {
    key: uuidv4(),
    content: <Image src={adidasForum} alt="adidas forum" width={1000} height={600} />,
  },
];
