import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from "@reduxjs/toolkit";

const initialState: UserState = {
  wishlist: [],
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    addItem: (state, action: PayloadAction<Sneaker>) => {
      state.wishlist = [...state.wishlist, action.payload];
    },
    removeItem: (state, action: PayloadAction<string>) => {
      state.wishlist = state.wishlist.filter(
        (item) => item.id !== action.payload
      );
    },
  },
});

export const { addItem, removeItem } = userSlice.actions;

export default userSlice.reducer;
