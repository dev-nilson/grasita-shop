import Topbar from "@/components/Topbar/Topbar";

export default function AuthLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <Topbar />
      <div
        style={{
          background: "#FAFAFA",
          height: "calc(100vh - 70px)",
          paddingTop: "25px",
        }}
      >
        {children}
      </div>
    </>
  );
}
