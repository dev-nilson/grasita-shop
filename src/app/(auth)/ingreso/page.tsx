"use client";

import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import LoginForm from "@/components/LoginForm/LoginForm";

export default function Page() {
  const user = useSelector((state: State) => state.auth.user);
  const router = useRouter();

  if (user) router.push("/");
  else return <LoginForm />;
}
