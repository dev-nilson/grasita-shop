"use client";

import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import SignupForm from "@/components/SignupForm/SignupForm";

export default function Page() {
  const user = useSelector((state: State) => state.auth.user);
  const router = useRouter();

  if (user) router.push("/");
  else return <SignupForm />;
}
