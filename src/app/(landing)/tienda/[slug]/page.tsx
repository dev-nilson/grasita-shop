import ProductDetails from "@/components/ProductDetails/ProductDetails";

export default function Page({ params }: { params: { slug: string } }) {
  return <ProductDetails slug={params.slug} />;
}
