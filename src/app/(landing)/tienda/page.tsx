"use client";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { db } from "@/firebase";
import {
  DocumentSnapshot,
  collection,
  onSnapshot,
  orderBy,
  query,
} from "firebase/firestore";
import Products from "@/components/Products/Products";

export default function Page() {
  const user = useSelector((state: State) => state.auth.user);
  const [products, setProducts] = useState<DocumentSnapshot[]>([]);

  useEffect(() => {
    return onSnapshot(
      query(collection(db, "store"), orderBy("timestamp", "desc")),
      (snapshot) => {
        setProducts(snapshot.docs);
      }
    );
  }, [user]);

  return <Products products={products} shopMode />;
}
