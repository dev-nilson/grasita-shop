"use client";
import { useEffect, useState } from "react";
import {
  DocumentSnapshot,
  collection,
  onSnapshot,
  orderBy,
  query,
  where,
} from "firebase/firestore";
import { useSearchParams } from "next/navigation";
import { useSelector } from "react-redux";
import { Parallax, ParallaxLayer } from "@react-spring/parallax";
import { db } from "@/firebase";
import sneaker from "../assets/shoe.png";
import box from "../assets/box.jpg";
import Image from "next/image";
import Dashboard from "@/components/Dashboard/Dashboard";
import WelcomeBanner from "@/components/WelcomeBanner/WelcomeBanner";
import Navbar from "@/components/Navbar/Navbar";
import Button from "@/components/Button/Button";

export default function Home() {
  const user = useSelector((state: State) => state.auth.user);
  const searchParams = useSearchParams();
  const wishlist = searchParams.get("lista") === "true" ? true : false;
  const [sneakers, setSneakers] = useState<DocumentSnapshot[]>([]);
  const [isFetching, setIsFetching] = useState(false);

  useEffect(() => {
    setIsFetching(true);
    if (!user) return;

    return onSnapshot(
      query(
        collection(db, "sneakers"),
        where("user", "==", user?.uid),
        orderBy("timestamp", "desc")
      ),
      (snapshot) => {
        setSneakers(snapshot.docs);
        setIsFetching(false);
      }
    );
  }, [user, user?.uid]);

  return (
    <main>
      {/* {!user && <Carousel />} */}
      {!user && (
        <Parallax pages={4}>
          <ParallaxLayer />
          <ParallaxLayer offset={1} />
          <ParallaxLayer offset={2} />
          <ParallaxLayer
            sticky={{ start: 0.2, end: 3 }}
          >
            <div className="split-container">
              <div className="split-view"></div>
              <div className="split-view">
                <Image
                  src={sneaker}
                  alt="nike air force 1 white"
                  style={{ width: "100%", height: "100%" }}
                />
              </div>
            </div>
          </ParallaxLayer>
          <ParallaxLayer offset={0.25} speed={2} style={{ padding: "0 10px" }}>
            <div className="split-container">
              <div className="split-view">
                <h1 className="text">
                  ¿Interesad@ en un par de sneakers pero los precios están por
                  las nubes?
                </h1>
              </div>
              <div className="split-view"></div>
            </div>
          </ParallaxLayer>
          <ParallaxLayer offset={1} speed={2} style={{ padding: "0 10px" }}>
            <div className="split-container">
              <div className="split-view">
                <h1 className="text">
                  ¿Interesad@ en un par de sneakers pero los precios están por
                  las nubes?
                </h1>
              </div>
              <div className="split-view"></div>
            </div>{" "}
          </ParallaxLayer>
          <ParallaxLayer offset={2} speed={2} style={{ padding: "0 10px" }}>
            <div className="split-container">
              <div className="split-view">
                <h1 className="text">
                  ¿Interesad@ en un par de sneakers pero los precios están por
                  las nubes?
                </h1>
              </div>
              <div className="split-view"></div>
            </div>{" "}
          </ParallaxLayer>
          <ParallaxLayer offset={3.3} speed={2} style={{ padding: "0 10px" }}>
            <div className="split-container">
              <Image
                src={box}
                alt="nike air force 1 white"
                style={{ width: "100%", height: "100%" }}
              />
            </div>
          </ParallaxLayer>
        </Parallax>
      )}
      {!isFetching && sneakers.length == 0 && !wishlist && <WelcomeBanner />}
      {((!isFetching && sneakers.length > 0) || wishlist) && (
        <>
          <Navbar />
          <Dashboard sneakers={sneakers} />
        </>
      )}
    </main>
  );
}
