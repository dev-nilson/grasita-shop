"use client";
import React from "react";
import Masonry from "react-masonry-css";
import styles from "./MasonryGrid.module.css";

const images = [
  {
    id: 0,
    alt: "",
    url: "https://i.pinimg.com/564x/c1/0f/b1/c10fb1bf9073d8bb80e42478fe03c586.jpg",
  },
  {
    id: 1,
    alt: "",
    url: "https://i.pinimg.com/564x/bb/95/af/bb95afede60583e271044c0c79ad72cb.jpg",
  },
  {
    id: 3,
    alt: "",
    url: "https://i.pinimg.com/736x/5e/c9/9d/5ec99d273e0d521e5c012bca326f5c30.jpg",
  },
  {
    id: 4,
    alt: "",
    url: "https://i.pinimg.com/564x/89/da/be/89dabe5acb10a10edf99979d98ac2f8c.jpg",
  },
];

export default function MasonryGrid() {
  return (
    <div>
      <Masonry className="masonry-grid" columnClassName="masonry-grid-column">
        {images.map((image) => (
          <div key={image.id}>
            <img src={image.url} alt={image.alt} className={styles.image} />
          </div>
        ))}
      </Masonry>
    </div>
  );
}
