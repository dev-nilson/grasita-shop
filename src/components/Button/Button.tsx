import { ButtonHTMLAttributes } from "react";
import styles from "./Button.module.css";

type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  variant: "primary" | "secondary";
};

export default function Button({ variant, children, ...props }: ButtonProps) {
  const buttonClasses = `${styles.button} ${
    variant === "primary" ? styles.primary : styles.secondary
  }`;

  return (
    <button className={buttonClasses} {...props}>
      {children}
    </button>
  );
}
