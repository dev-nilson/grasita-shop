"use client";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { signInWithEmailAndPassword } from "firebase/auth";
import { login } from "@/redux/slices/authSlice";
import { auth } from "@/firebase";
import Link from "next/link";
import Input from "../Input/Input";
import Button from "../Button/Button";
import styles from "./LoginForm.module.css";

export default function LoginForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginData>();
  const dispatch = useDispatch();
  const [loginError, setLoginError] = useState<string | null>(null);
  const [showPassword, setShowPassword] = useState(false);

  const onSubmit = async (data: LoginData) => {
    signInWithEmailAndPassword(auth, data.email, data.password)
      .then((userCredential) => {
        dispatch(
          login({
            displayName: userCredential.user.displayName,
            email: userCredential.user.email,
            uid: userCredential.user.uid,
            emailVerified: userCredential.user.emailVerified,
          })
        );
      })
      .catch((error) => {
        let errorMessage = "";

        if (error.code == "auth/user-not-found")
          errorMessage = "Correo electronico no registrado";
        else if (error.code == "auth/invalid-email")
          errorMessage = "Correo electornico es invalido";
        else if (error.code == "auth/wrong-password")
          errorMessage = "Contraseña es invalida";

        setLoginError(errorMessage);
      });
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.container}>
        <h3>Iniciar sesion</h3>
        <div className={styles.group}>
          <Input
            label="email"
            register={register}
            required
            placeholder="Correo Electronico"
          />
          {errors.email?.type === "required" && (
            <small className={styles.error}>
              El correo electronico es obligatorio
            </small>
          )}
        </div>
        <div className={styles.group}>
          <Input
            type={`${showPassword ? "text" : "password"}`}
            label="password"
            placeholder="Contreseña"
            register={register}
            required
            secret
            setShowSecret={setShowPassword}
          />
          {errors.password?.type === "required" && (
            <small className={styles.error}>La contraseña es obligatoria</small>
          )}
        </div>
        <Button variant="primary" type="submit">
          Ingresar
        </Button>
        {loginError && <small className={styles.warning}>{loginError}</small>}
        <p className={styles.text}>
          ¿No tienes cuenta?{" "}
          <Link className={styles.link} href="registro">
            Crear cuenta
          </Link>
        </p>
      </div>
    </form>
  );
}
