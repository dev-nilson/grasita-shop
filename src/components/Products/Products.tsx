import { DocumentData } from "firebase/firestore";
import Link from "next/link";
import ProductCard from "../ProductCard/ProductCard";
import ProductCardSkeleton from "../ProductCardSkeleton/ProductCardSkeleton";
import styles from "./Products.module.css";

type ProductsProps = {
  products: DocumentData[];
  shopMode?: boolean;
};

export default function Products({ products, shopMode }: ProductsProps) {
  return (
    <div className={styles.products}>
      <div className={styles.container}>
        {products.length > 0
          ? products.map((product: DocumentData) =>
              shopMode ? (
                <Link
                  key={product.id}
                  className={styles.link}
                  href={`/tienda/${product.data().slug}`}
                >
                  <ProductCard
                    id={product.id}
                    title={product.data().brand + " " + product.data().model}
                    price={product.data().price}
                    image={product.data().image}
                    size={product.data().size}
                  />
                </Link>
              ) : (
                <ProductCard
                  key={product.id}
                  id={product.id}
                  image={
                    "https://images.stockx.com/images/New-Balance-550-White-Mint-Green-Product.jpg?fit=fill&bg=FFFFFF&w=1200&h=857&fm=jpg&auto=compress&dpr=2&trim=color&updated_at=1651519339&q=75"
                  }
                  title={product.data().brand + " " + product.data().model}
                  price={product.data().price}
                  size={product.data().size}
                />
              )
            )
          : Array.from({ length: 5 }, (_, index) => (
              <ProductCardSkeleton key={index} />
            ))}
      </div>
    </div>
  );
}
