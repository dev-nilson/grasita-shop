"use client";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { collection, addDoc, serverTimestamp } from "firebase/firestore";
import { addItem } from "@/redux/slices/userSlice";
import { db } from "@/firebase";
import Input from "../Input/Input";
import Button from "../Button/Button";
import styles from "./WishlistForm.module.css";

export default function WishlistForm() {
  const user = useSelector((state: State) => state.auth.user);
  const dispatch = useDispatch();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<WishlistData>();

  const onSubmit = async (data: WishlistData) => {
    const sneakerToAdd = {
      brand: data.brand,
      model: data.model,
      size: data.size,
      timestamp: serverTimestamp(),
      user: user!.uid,
      image: "",
    };

    const docRef = await addDoc(collection(db, "sneakers"), sneakerToAdd);
    dispatch(addItem({ ...sneakerToAdd, id: docRef.id }));
    reset();
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.container}>
        <h3>Lista</h3>
        <div className={styles.group}>
          <Input
            label="brand"
            register={register}
            required
            placeholder="Marca"
          />
          {errors.brand?.type === "required" && (
            <small className={styles.error}>La marca es obligatoria</small>
          )}
          {errors.brand?.type === "pattern" && (
            <small className={styles.error}>La marca no es valida</small>
          )}
        </div>
        <div className={styles.group}>
          <Input
            label="model"
            register={register}
            required
            placeholder="Modelo"
          />
          {errors.model?.type === "required" && (
            <small className={styles.error}>El modelo es obligatorio</small>
          )}
          {errors.model?.type === "pattern" && (
            <small className={styles.error}>El modelo no es valido</small>
          )}
        </div>
        <div className={styles.group}>
          <Input
            label="size"
            register={register}
            required
            placeholder="Talla"
          />
          {errors.size?.type === "required" && (
            <small className={styles.error}>La talla es obligatoria</small>
          )}
          {errors.size?.type === "pattern" && (
            <small className={styles.error}>La talla no es valida</small>
          )}
        </div>
        <div className={styles.group}>
          <Input
            label="image"
            register={register}
            required={false}
            placeholder="Imagen"
          />
        </div>
        <Button variant="primary" type="submit">
          Agregar
        </Button>
      </div>
    </form>
  );
}
