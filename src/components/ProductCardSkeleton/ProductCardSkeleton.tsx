"use client";
import styles from "./ProductCardSkeleton.module.css";

export default function ProductCardSkeleton() {
  return (
    <div className={styles.card}>
      <div className={styles.container}>
        <div className={styles.header} />
        <div className={styles.body} />
        <div className={styles.footer} />
      </div>
    </div>
  );
}
