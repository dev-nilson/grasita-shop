"use client";
import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import Button from "../Button/Button";
import MasonryGrid from "../MasonryGrid/MasonryGrid";
import styles from "./WelcomeBanner.module.css";

export default function WelcomeBanner() {
  const user = useSelector((state: State) => state.auth.user);
  const router = useRouter();

  return (
    <div className={styles.banner}>
      <div className={styles.left}>
        {user?.displayName && (
          <div className={styles.text}>
            <h1 className={styles.title}>👋 Bienvenid@, {user.displayName}</h1>
            <p className={styles.caption}>Crea tu lista de deseos</p>
          </div>
        )}
        <Button variant="primary" onClick={() => router.push("?lista=true")}>
          Crear lista
        </Button>
      </div>
      <div className={styles.right}>
        <MasonryGrid />
      </div>
    </div>
  );
}
