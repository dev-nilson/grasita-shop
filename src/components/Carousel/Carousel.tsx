"use client";
import { useEffect, useState } from "react";
import { config } from "react-spring";
import { sneakers } from "@/utils/data";
import Carousel from "react-spring-3d-carousel";
import styles from "./Carousel.module.css";

export default function CarouselComponent() {
  const [settings, setSettings] = useState({
    goToSlide: 0,
    offsetRadius: 2,
    showNavigation: false,
    config: config.stiff,
  });

  useEffect(() => {
    const interval = setInterval(() => {
      setSettings((prevSettings) => ({
        ...prevSettings,
        goToSlide: (prevSettings.goToSlide + 1) % 5,
      }));
    }, 3000);

    return () => clearInterval(interval);
  }, []);

  return (
    <div className={styles.carousel}>
      <Carousel
        slides={sneakers}
        goToSlide={settings.goToSlide}
        offsetRadius={settings.offsetRadius}
        showNavigation={settings.showNavigation}
        animationConfig={settings.config}
      />
    </div>
  );
}
