"use client";
import { useEffect, useState } from "react";
import { collection, onSnapshot, query, where } from "firebase/firestore";
import { db } from "@/firebase";
import { Ring } from "@uiball/loaders";
import Options from "../Option/Option";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import styles from "./ProductDetails.module.css";

type ProductDetailsProps = {
  slug: string;
};

export default function ProductDetails({ slug }: ProductDetailsProps) {
  const [sneaker, setSneaker] = useState<Sneaker>();
  const [isFetching, setIsFetching] = useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    setIsFetching(true);

    return onSnapshot(
      query(collection(db, "store"), where("slug", "==", slug)),
      (snapshot) => {
        setSneaker(snapshot.docs[0].data() as Sneaker);
        setIsFetching(false);
      }
    );
  }, [slug]);

  return (
    <div className={styles.productDetails}>
      {isFetching ? (
        <Ring />
      ) : (
        <>
          <div className={styles.container}>
            <div className={styles.left}>
              <img className={styles.image} src={sneaker?.image} />
            </div>
            <div className={styles.right}>
              <div className={styles.info}>
                <h1 className={styles.title}>{sneaker?.brand}</h1>
                <p className={styles.subtitle}>{sneaker?.model}</p>
              </div>
              <Options
                data={[
                  { value: "8", label: "8 US" },
                  { value: "9", label: "9 US" },
                ]}
              />
              <div className={styles.controls}>
                <Button
                  variant="secondary"
                  onClick={() => setIsModalOpen(true)}
                >
                  Sugerir Talla
                </Button>
                <Button variant="primary">Comprar</Button>
              </div>
            </div>
          </div>
          {isModalOpen && sneaker && (
            <Modal sneaker={sneaker} setIsModalOpen={setIsModalOpen} />
          )}
        </>
      )}
    </div>
  );
}
