"use client";
import { MouseEvent } from "react";
import { useDispatch } from "react-redux";
import { removeItem } from "@/redux/slices/userSlice";
import { doc, deleteDoc } from "firebase/firestore";
import { db } from "@/firebase";
import Image from "next/image";
import styles from "./ProductCard.module.css";

type ProductsCardProps = {
  id: string;
  image?: string;
  title: string;
  price?: number;
  size: number;
};

export default function ProductCard({
  id,
  image,
  title,
  price,
  size,
}: ProductsCardProps) {
  const dispatch = useDispatch();

  const deleteProduct = async (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    e.stopPropagation();
    dispatch(removeItem(id));
    await deleteDoc(doc(db, "sneakers", id));
  };

  return (
    <div className={styles.card}>
      <div className={styles.container}>
        {!!!price && (
          <button className={styles.close} onClick={deleteProduct}>
            <svg
              viewBox="0 0 64 64"
              fill="currentColor"
              height="22px"
              width="22px"
            >
              <path
                fill="none"
                stroke="currentColor"
                strokeMiterlimit={10}
                strokeWidth={2}
                d="M18.947 17.153l26.098 25.903M19.045 43.153l25.902-26.097"
              />
            </svg>
          </button>
        )}
        <div className={styles.header}>
          {image && <img className={styles.image} src={image} alt={title} />}
        </div>
        <div className={styles.body}>
          <p className={styles.title} title={title}>
            {title}
          </p>
          {!!price && <p className={styles.price}>${price}</p>}
          <div className={styles.chip}>
            <small>Talla: {size} US</small>
          </div>
        </div>
      </div>
    </div>
  );
}
