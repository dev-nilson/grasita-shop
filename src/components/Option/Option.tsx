import styles from "./Option.module.css";

type OptionsProps = {
  data: Option[];
};

export default function Options({ data }: OptionsProps) {
  return (
    <div className={styles.options}>
      {data.map((item) => {
        return (
          <>
            <input
              className={styles.input}
              type="radio"
              id={item.value}
              name="option"
              value={item.value}
            />
            <label className={styles.label} htmlFor={item.value}>
              {item.label}
            </label>
          </>
        );
      })}
    </div>
  );
}
