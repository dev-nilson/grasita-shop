import Link from "next/link";
import styles from "./Topbar.module.css";

export default function Topbar() {
  return (
    <nav className={styles.topbar}>
      <div className={styles.container}>
        <Link className={styles.link} href="/">
          <h1>GrasitaShop</h1>
        </Link>
      </div>
    </nav>
  );
}
