import { useForm } from "react-hook-form";
import { addDoc, collection } from "firebase/firestore";
import { db } from "@/firebase";
import Input from "../Input/Input";
import Button from "../Button/Button";
import styles from "./SuggestionForm.module.css";

type SuggestionFormProps = {
  sneaker: Sneaker;
};

export default function SuggestionForm({ sneaker }: SuggestionFormProps) {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ModalData>();

  const onSubmit = async (data: ModalData) => {
    const requestToAdd = {
      brand: sneaker.brand,
      model: sneaker.model,
      size: data.size,
      // TODO: add user
    };

    try {
      await addDoc(collection(db, "requests"), requestToAdd);
    } catch (err) {
      console.log(err);
    }
    reset();
  };

  return (
    <form
      className={styles.modal}
      onSubmit={() => handleSubmit(onSubmit)}
      onClick={(e) => e.stopPropagation()}
    >
      <div className={styles.group}>
        <Input
          label="size"
          placeholder="Talla US"
          register={register}
          type="number"
          min={1}
          max={20}
          required
        />
        {errors.size?.type === "required" && (
          <small className={styles.error}>La talla es obligatoria</small>
        )}
      </div>
      <Button variant="primary" type="submit">
        Enviar
      </Button>
    </form>
  );
}
