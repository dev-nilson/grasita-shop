import { InputHTMLAttributes, useEffect, useState } from "react";
import { UseFormRegister } from "react-hook-form";
import styles from "./Input.module.css";

type InputProps<T> = {
  label: T extends UseFormRegister<WishlistData>
    ? "brand" | "model" | "size" | "image"
    : T extends UseFormRegister<LoginData>
    ? "email" | "password"
    : T extends UseFormRegister<SignupData>
    ? "name" | "email" | "password" | "confirmation"
    : T extends UseFormRegister<ModalData>
    ? "size"
    : never;
  register: T;
  required: boolean;
  secret?: boolean;
  setShowSecret?: any;
  validation?: any;
} & InputHTMLAttributes<HTMLInputElement>;

export default function Input({
  label,
  register,
  required,
  validation,
  secret,
  setShowSecret,
  ...props
}: InputProps<any>) {
  const [isEmpty, setIsEmpty] = useState(true);
  const [type, setType] = useState(props.type);

  useEffect(() => {
    setType(props.type);
  }, [props.type]);

  return (
    <div className={styles.container}>
      <input
        className={styles.input}
        {...register(label, { required, pattern: /\S/g, ...validation })}
        {...props}
        onChange={(e) => {
          if (e.target.value === "") {
            setIsEmpty(true);
          } else {
            setIsEmpty(false);
          }
        }}
      />
      {secret && setShowSecret && !isEmpty && (
        <button
          className={styles.button}
          type="button"
          onClick={() => setShowSecret((prev: any) => !prev)}
        >
          {type === "password" ? "Mostrar" : "Esconder"}
        </button>
      )}
    </div>
  );
}
