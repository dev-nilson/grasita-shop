"use client";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import {
  createUserWithEmailAndPassword,
  sendEmailVerification,
  updateProfile,
} from "firebase/auth";
import { login } from "@/redux/slices/authSlice";
import { auth } from "@/firebase";
import Link from "next/link";
import Input from "../Input/Input";
import Button from "../Button/Button";
import styles from "./SignupForm.module.css";

export default function SignupForm() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SignupData>();
  const dispatch = useDispatch();
  const [signupError, setSignupError] = useState<string | null>(null);
  const [showPassword, setShowPassword] = useState(false);

  const onSubmit = async (data: SignupData) => {
    createUserWithEmailAndPassword(auth, data.email, data.password)
      .then((userCredential) => {
        sendEmailVerification(userCredential.user);

        updateProfile(auth.currentUser!, {
          displayName: data.name,
        });

        dispatch(
          login({
            displayName: data.name,
            email: userCredential.user.email,
            uid: userCredential.user.uid,
            emailVerified: userCredential.user.emailVerified,
          })
        );
      })
      .catch((error) => {
        let errorMessage = "";

        if (error.code == "auth/email-already-in-use")
          errorMessage = "Correo electronico ya registrado";
        else if (error.code == "auth/invalid-email")
          errorMessage = "Correo electronico es invalido";

        setSignupError(errorMessage);
      });
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
      <div className={styles.container}>
        <h3>Crear cuenta</h3>
        <div className={styles.group}>
          <Input
            label="name"
            register={register}
            required
            placeholder="Nombre"
          />
          {errors.name?.type === "required" && (
            <small className={styles.error}>El nombre es obligatorio</small>
          )}
        </div>
        <div className={styles.group}>
          <Input
            label="email"
            register={register}
            required
            placeholder="Correo Electronico"
          />
          {errors.email?.type === "required" && (
            <small className={styles.error}>
              El correo electronico es obligatorio
            </small>
          )}
        </div>
        <div className={styles.group}>
          <Input
            type={`${showPassword ? "text" : "password"}`}
            label="password"
            placeholder="Contreseña"
            register={register}
            required
            secret
            setShowSecret={setShowPassword}
          />
          {errors.password?.type === "required" && (
            <small className={styles.error}>La contraseña es obligatoria</small>
          )}
        </div>
        <Button variant="primary" type="submit">
          Registrar
        </Button>
        {signupError && <small className={styles.warning}>{signupError}</small>}
        <p className={styles.text}>
          ¿Ya tienes cuenta?{" "}
          <Link className={styles.link} href="ingreso">
            Iniciar sesion
          </Link>
        </p>
      </div>
    </form>
  );
}
