import SuggestionForm from "../SuggestionForm/SuggestionForm";
import styles from "./Modal.module.css";

type ModalProps = {
  sneaker: Sneaker;
  setIsModalOpen: any;
};

export default function Modal({ sneaker, setIsModalOpen }: ModalProps) {
  return (
    <div
      className={styles.backdrop}
      onClick={(e) => {
        setIsModalOpen(false);
      }}
    >
      <SuggestionForm sneaker={sneaker} />
    </div>
  );
}
