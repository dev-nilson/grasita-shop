"use client";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { signOut } from "firebase/auth";
import { auth } from "@/firebase";
import { logout } from "@/redux/slices/authSlice";
import Link from "next/link";
import Button from "../Button/Button";
import styles from "./Navbar.module.css";

export default function Navbar() {
  const user = useSelector((state: State) => state.auth.user);
  const [menuOpen, setMenuOpen] = useState(false);
  const dispatch = useDispatch();

  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  return (
    <nav className={styles.navbar}>
      <div className={styles.container}>
        <div className={styles.logo}>
          <Link className={styles.link} href="/">
            <h1>GrasitaShop</h1>
          </Link>
        </div>
        {!user && (
          <ul className={`${styles.menu}`}>
            <li className={styles.item}>
              <Link className={styles.link} href="/tienda">
                Tienda
              </Link>
            </li>
          </ul>
        )}
        <div className={styles.controls}>
          {!user ? (
            <>
              <Link href="/ingreso">
                <Button variant="secondary">Ingreso</Button>
              </Link>
              <Link href="/registro">
                <Button variant="primary">Registro</Button>
              </Link>
            </>
          ) : (
            <img
              className={styles.avatar}
              src={`https://api.dicebear.com/6.x/initials/svg?seed=${user.displayName}&backgroundColor=000000&scale=75`}
              onClick={() => {
                signOut(auth);
                dispatch(logout());
              }}
            />
          )}
        </div>
      </div>
      <div className={styles.mobileContainer}>
        <div onClick={toggleMenu}>
          <div className={`${styles.hamburger} ${menuOpen ? styles.open : ""}`}>
            <span className={styles.line}></span>
            <span className={styles.line}></span>
            <span className={styles.line}></span>
          </div>
        </div>
        <div className={styles.logo}>
          <Link className={styles.link} href="/">
            <h1>GrasitaShop</h1>
          </Link>
        </div>
      </div>
    </nav>
  );
}
