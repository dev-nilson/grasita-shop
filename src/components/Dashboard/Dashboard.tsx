import { DocumentSnapshot } from "firebase/firestore";
import Products from "../Products/Products";
import WishlistForm from "../WishlistForm/WishlistForm";
import styles from "./Dashboard.module.css";

type DashboardProps = {
  sneakers: DocumentSnapshot[];
};

export default function Dashboard({ sneakers }: DashboardProps) {
  return (
    <div className={styles.dashboard}>
      <div className={styles.container}>
        <div className={styles.left}>
          {sneakers.length === 0 ? (
            <div className={styles.info}>
              <h2>Agrega sneakers a tu lista</h2>
            </div>
          ) : (
            <Products products={sneakers} />
          )}
        </div>
        <div className={styles.right}>
          <WishlistForm />
        </div>
      </div>
    </div>
  );
}
