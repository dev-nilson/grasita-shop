type User = {
  displayName: string | null;
  email: string | null;
  uid: string;
  emailVerified: boolean;
} | null;

type WishlistData = {
  brand: string;
  model: string;
  size: string;
  image: string;
};

type ModalData = {
  size: string;
};

type LoginData = {
  email: string;
  password: string;
};

type SignupData = {
  name: string;
  email: string;
  password: string;
  confirmation: string;
};

type Sneaker = {
  id: string;
  brand: string;
  model: string;
  image: string;
  size: string;
  timestamp: any;
  user: string;
};

type AuthState = {
  user: User;
};

type UserState = {
  wishlist: Sneaker[];
};

type State = {
  auth: AuthState;
};

type Option = {
  label: string;
  value: string;
};
